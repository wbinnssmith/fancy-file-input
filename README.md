Fancy File Input
=================

This jQuery plugin provides an enormous amount of flexibility for styling file input fields. The visual appearance is 100% customisable via CSS. No images are used so they display nicely on retina/HiDPI displays. They are responsive, accessible, and support internationalization (i18n).

## Browser support

IE9, IE10, IE11, Firefox, Chrome, Safari, Opera, iOS6+, Android 3+, Windows Phone 8*

*the styling works, however Windows Phone 8 does not yet allow access to files, so they are effectively disabled. 

## How does it work?

Wrap your file input with a label with the ffi class and data attribute for the button text. If you can't modify the markup, or want to render this clientside first, then the JS will wrap it for you, however this can lead to a flash of unstyled content for the field and, depending on the height of the field, may cause some "jank" in the page.

    <label class="ffi" data-ffi-button-text="Browse...">
        <input type="file" name="something" />
    </label>

[Take a look at the demos](http://seancurtis.com/experiments/ffi/) for more configurations and customisations.

## Basic Usage

    $(function(){
		$('input[type=file]').fancyFileInput();
	});

## Customise with options

    $(function(){
        $('input[type=file]').fancyFileInput({
            buttonText: "Browse\u2026", // The text for the "button"
            multipleFileTextPattern: "{0} files", // Shown when multiple files are chosen
            clearButtonText: "Clear" // For screen readers
        });
    });