(function($) {
  /*
    ======== A Handy Little QUnit Reference ========
    http://api.qunitjs.com/

    Test methods:
      module(name, {[setup][ ,teardown]})
      test(name, callback)
      expect(numberOfAssertions)
      stop(increment)
      start(decrement)
    Test assertions:
      ok(value, [message])
      equal(actual, expected, [message])
      notEqual(actual, expected, [message])
      deepEqual(actual, expected, [message])
      notDeepEqual(actual, expected, [message])
      strictEqual(actual, expected, [message])
      notStrictEqual(actual, expected, [message])
      throws(block, [expected], [message])
  */

  module('jQuery#fancyFileInput', {
    setup: function() {
      this.elems = $('#qunit-fixture').children();
    }
  });

  test('is chainable', 1, function() {
    // Not a bad test to run on collection methods.
    strictEqual(this.elems.fancyFileInput(), this.elems, 'should be chainable');
  });

  test('instance is accessible via jQuery\'s data()', 2, function () {
    this.elems.fancyFileInput();
    strictEqual(typeof this.elems.first().data('FancyFileInput'), 'object', 'should be an object');
    strictEqual(this.elems.get(0), this.elems.first().data('FancyFileInput').el, 'element the instance refers to should match the one that was pluginified');
  });

  test('instance is accessible via jQuery\'s data() after clear()', 3, function () {
    this.elems.fancyFileInput();
    strictEqual(typeof this.elems.first().data('FancyFileInput'), 'object', 'should be an object');
    strictEqual(this.elems.get(0), this.elems.first().data('FancyFileInput').el, 'element the instance refers to should match the one that was pluginified');
    this.elems.first().data('FancyFileInput').clear();
    strictEqual(typeof this.elems.first().data('FancyFileInput'), 'object', 'should be an object');
  });

}(jQuery));